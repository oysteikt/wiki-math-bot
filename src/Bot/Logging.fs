namespace Bot

module Logging =

  open System

  let log message = 
    printfn "[%A] [Info] %s" DateTime.Now message