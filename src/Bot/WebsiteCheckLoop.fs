namespace Bot

module WebsiteCheckLoop =

  open Logging
  open DSharpPlus
  open Channels

  let private sendToChannelWith (client:DiscordClient) message id = 
    log <| sprintf "Sending message to channel: %A" id
    id
    |> client.GetChannelAsync
    |> Async.AwaitTask
    |> Async.RunSynchronously
    |> fun channel -> 
          client.SendMessageAsync(channel, message)
          |> Async.AwaitTask
          |> Async.RunSynchronously
    |> ignore
  
  let private formatMessage message link =
    message + "\n" + link

  let scrapeFun client (previousListOfResults:List<string * string>) (listOfResults:List<string * string>) =
    log "Scraping website"

    match previousListOfResults, listOfResults with
    | (previousListOfResults, listOfResults) when previousListOfResults <> listOfResults ->

      channels
      |> Seq.iter (fun id -> sendToChannelWith client (formatMessage <|| Seq.head listOfResults) id)

      Seq.head listOfResults
      ||> formatMessage
      |> fun s -> s.Split("\n")
      |> Seq.map (fun s -> "\t" + s)
      |> Seq.fold (fun a b -> a + "\n" + b) ""
      |> sprintf "Found following update: \n%s" 
      |> log

    | (_, _) ->
      log "No new content found"

    listOfResults