namespace Bot

module Config = 

  open System.IO
  open Microsoft.Extensions.Configuration

  let private getConfig =
    let builder = new ConfigurationBuilder()
    do builder.SetBasePath( Directory.GetCurrentDirectory() + "/data" ) |> ignore
    do builder.AddJsonFile("config.json") |> ignore
    builder.Build()

  let config = getConfig