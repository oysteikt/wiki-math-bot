namespace Bot

module Channels =

  open FSharp.Data
  open System.IO

  let private filepath = "./data/channels.dat"

  let mutable channels = 
    File.ReadLines(filepath)
      |> Seq.map (fun line -> line.ToString().AsInteger64())
      |> Seq.map (uint64)
      |> set

  let private updateChannels newChannels =
    newChannels
    |> Seq.map (fun i -> i.ToString())
    |> Seq.toList
    |> fun lines -> File.WriteAllLines(filepath, lines)
    channels <- newChannels

  let private removeChannel (channelId:uint64) =
    channels.Remove(channelId)
    |> updateChannels

  let private addChannel (channelId:uint64) =
    channels.Add(channelId)
    |> updateChannels
  
  let toggleChannel (channelId:uint64) =
    match channelId with
    | channelId when channels.Contains(channelId) -> 
      removeChannel channelId
      false

    | channelId -> 
      addChannel channelId
      true
  
    