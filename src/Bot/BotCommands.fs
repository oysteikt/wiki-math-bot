namespace Bot

module BotCommands = 

  open System.Threading.Tasks
  open DSharpPlus.CommandsNext
  open DSharpPlus.CommandsNext.Attributes

  open Channels
  open Logging
  
  type BotCommands() =
    [<Command("hi")>]
    member public self.hi(ctx:CommandContext) = 
      async { ctx.RespondAsync "Hi there" |> ignore } |> Async.StartAsTask :> Task       
  
    [<Command("echo")>]
    member public self.echo(ctx:CommandContext) (message:string) =
      async { ctx.RespondAsync message |> ignore } |> Async.StartAsTask :> Task 
  
    [<Command("toggle")>]
    member public self.toggle(ctx:CommandContext) =
      log <| sprintf "!toggle from %A" ctx.Channel.Id
      async { 
        toggleChannel ctx.Channel.Id
        |> fun channelGotAdded ->
           match channelGotAdded with
           | true  -> 
              log <| sprintf "Added %A to list of channels " ctx.Channel.Id
              ctx.RespondAsync "This is now my channel :)"   |> ignore
           | false ->
              log <| sprintf "Removed %A from list of channels " ctx.Channel.Id
              ctx.RespondAsync "This is now your channel (:" |> ignore
      } |> Async.StartAsTask :> Task 
