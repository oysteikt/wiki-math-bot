namespace Bot

module WikiMathParser =

  open System
  open FSharp.Data
  open Config

  let private page = HtmlDocument.Load(sprintf "https://wiki.math.ntnu.no/%s/%s/start" config.["Class"] config.["Year"])

  let private findPDFLink (node:HtmlNode) = 
    node.CssSelect(".mf_pdf") 
    |> fun l -> match l with
                | l when Seq.length l = 0 -> "" 
                | l -> HtmlNode.attributeValue "href" (Seq.head l)
                       |> (+) "https://wiki.math.ntnu.no"
  
  let getStatus = 
    (page.CssSelect ".level2 > ul > .level1")
    |> Seq.map (fun (x:HtmlNode) -> (x.InnerText().TrimStart(), findPDFLink x ))
    |> Seq.filter (fun (x:string, _) -> x.Contains("Problem Set"))
    |> Seq.toList


  let private timer = new Timers.Timer( config.["SecondsBetweenUpdate"].AsFloat() * 1000.0)
  let private waitAPeriod = Async.AwaitEvent (timer.Elapsed) |> Async.Ignore

  let scrapePeriodically (callback: List<String * String> -> unit ) = 
    timer.Start()
    async {
      while true do
        Async.RunSynchronously waitAPeriod
        getStatus
        |> callback
    }
