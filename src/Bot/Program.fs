namespace Bot

module core = 

  open System
  open DSharpPlus
  open DSharpPlus.CommandsNext
  open System.Threading.Tasks

  open Config
  open WebsiteCheckLoop
  open BotCommands
  open WikiMathParser

  let getDiscordConfig =
    let conf = new DiscordConfiguration()
    conf.set_Token config.["BotToken"]
    conf.set_TokenType TokenType.Bot
    conf.set_UseInternalLogHandler true
    // conf.set_LogLevel LogLevel.Debug
    conf

  let getCommandsConfig =
    let conf = new CommandsNextConfiguration()
    conf.set_StringPrefix "!"
    conf

  let client =  new DiscordClient(getDiscordConfig)
  let commands = client.UseCommandsNext(getCommandsConfig)

  let mainTask =
    let mutable previousResults = getStatus

    async {
      commands.RegisterCommands<BotCommands>()
      client.ConnectAsync() |> Async.AwaitTask |> Async.RunSynchronously
      do! scrapePeriodically (scrapeFun client previousResults
                        >> fun results -> previousResults <- results)
      do! Async.AwaitTask(Task.Delay(-1))
    }

  [<EntryPoint>]
  let main argv =
    Async.RunSynchronously(mainTask)
    0