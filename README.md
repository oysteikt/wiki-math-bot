# WikiMathBot

This is a bot originally made for keeping track of whenever the newest exercise came out for the subject `MA0301`

In order to make the bot functional, it needs a configuration file at `src/Bot/config.json` that looks like this:

```json
{
  "BotToken": "<secret discord bot token>",
  "Class": "ma0301",
  "Year": "2021v",
  "SecondsBetweenUpdate": 3600.0
}
```

The class and year variables are part of the url to the page:

`https://wiki.math.ntnu.no/<class>/<year>/start`

The project can be run by executing `dotnet run` from within the `src/Bot` directory
